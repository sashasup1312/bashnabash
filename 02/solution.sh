#!/bin/bash
echo '228.com 228.ru' > file
mkdir -p a1/11/{1,11} a2/22/{2,22,222} b1/33/3
find . -type d -exec cp file {} \; 2>/dev/null
find . \( -type f -name solution.sh -prune \) -o -type f -print0 | xargs -0 sed -i 's/228.com/229.com/g'
echo "Show the result:"
find . -name file -print0 -exec cat "{}" \;
exit 0
